##Run application and tests.

##Before you start

* install dependencies ```npm install```


## Tests

* run tests ```npm test```
* If you want to create coverage, run ```npm run coverage``` . To see it, open file **./tests/coverage/lcov-report/index.html**

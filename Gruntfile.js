/*global module*/
module.exports = function (grunt)
{
    'use strict';

    var jenkinsOptions = {
        reporter: 'checkstyle',
        reporterOutput: 'target/jshint.xml'
    };

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: true,
                src: ['app/**/*.js', 'tests/**/*.js']
            },
            default: ['<%=jshint.options.src%>'],
            jenkins: {
                options: jenkinsOptions,
                src: ['<%=jshint.options.src%>']
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-jshint');
};

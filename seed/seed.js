(function ()
{
    var fs = require('fs'),
            Promise = require('bluebird'),
            ActivateTokenModel = require('./../app/DAO/ActivateTokenAndResetDAO').model,
            AlertModel = require('./../app/DAO/AlertDAO').model,
            DeviceModel = require('./../app/DAO/DeviceDAO').model,
            EmailModel = require('./../app/DAO/EmailDAO').model,
            GroupModel = require('./../app/DAO/GroupDAO').model,
            PasswordModel = require('./../app/DAO/PasswordDAO').model,
            ProductModel = require('./../app/DAO/ProductDAO').model,
            TokenModel = require('./../app/DAO/TokenDAO').model,
            UserModel = require('./../app/DAO/UserDAO').model,
            VendorModel = require('./../app/DAO/VendorDAO').model,
            pwd = require('process-pwd');

    module.exports = function (url)
    {
        return new Promise(function (resolve, reject)
        {
            fs.readFile(url, {encoding: 'UTF-8'}, function (error, data)
            {

                if (error) {
                    console.log('errorRead', error);
                    reject(error);
                }
                data = JSON.parse(data);
                ActivateTokenModel.removeQ({}).then(function ()
                {
                    return AlertModel.removeQ({});
                }).then(function ()
                {
                    return DeviceModel.removeQ({});
                }).then(function ()
                {
                    return EmailModel.removeQ({});
                }).then(function ()
                {
                    return GroupModel.removeQ({});
                }).then(function ()
                {
                    return PasswordModel.removeQ({});
                }).then(function ()
                {
                    return ProductModel.removeQ({});

                }).then(function ()
                {
                    return TokenModel.removeQ({});
                }).then(function ()
                {
                    return UserModel.removeQ({});
                }).then(function ()
                {
                    return VendorModel.removeQ({});
                }).then(function ()
                {
                    console.log('finish remove');
                    return AlertModel.createQ(data.alert)
                }).then(function ()
                {

                    return VendorModel.createQ(data.vendor);
                }).then(function ()
                {

                    return ProductModel.createQ(data.product);
                }).then(function ()
                {

                    return UserModel.createQ(data.users);
                }).then(function ()
                {
                    return PasswordModel.createQ(data.password);
                }).then(function ()
                {
                    console.log('success');
                    resolve();
                }).catch(function (error)
                {
                    reject(error);
                    console.log('error');
                    console.log(error);
                })
            });
        })

    }

})();

(function ()
{

    var moment = require('moment-timezone'),
            date = null;
    moment.tz.setDefault('UTC');


    function get()
    {
        return date || new Date().getTime();
    }

    function set(timestamp)
    {
        date = timestamp;
    }

    module.exports = {
        set: set,
        get: get,
        moment: moment
    }

})();

(function ()
{
    'use strict';
    var security = require('./security'),
            VendorDAO = require('../DAO/VendorDAO'),
            applicationException = require('../services/applicationException');

    function create(context)
    {
        function search()
        {
            return security.isAuthenticated(context).then(function ()
            {
                return VendorDAO.search();
            });
        }

        function createOrUpdate(vendor)
        {
            return security.isAdmin(context).then(function ()
            {
                if (!vendor || !vendor.hasOwnProperty('name')) {
                    throw applicationException.new(applicationException.PRECONDITION_FAILED);
                }
                return VendorDAO.createOrUpdate(vendor);
            });
        }

        function remove(id)
        {
            return security.isAdmin(context).then(function ()
            {
                return VendorDAO.remove(id);
            });
        }

        return {
            search: search,
            remove: remove,
            createOrUpdate: createOrUpdate
        }
    }

    module.exports = {
        create: create
    }
})();

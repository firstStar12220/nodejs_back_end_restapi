(function ()
{
    'use strict';

    var Promise = require('bluebird'),
            _ = require('lodash'),
            applicationException = require('../services/applicationException');

    function isAuthenticated(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!context || !context.user) {
                reject(applicationException.new(applicationException.UNAUTHORIZED, 'User does not exist'));
            } else {
                resolve();
            }
        })

    }

    function isAdmin(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!context || !context.user || !context.user.admin) {
                reject(applicationException.new(applicationException.FORBIDDEN, 'You don\'t have access to this page or resource'));
            } else {
                resolve();
            }
        })
    }

    function checkAuthor(userId, context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!context || !context.user || userId !== context.user.id.toString()) {
                reject(applicationException.new(applicationException.FORBIDDEN));
            } else {
                resolve();
            }
        });

    }

    function checkRoles(context)
    {
        var user = context && context.user;
        return new Promise(function (resolve, reject)
        {
            if (!user || !user.role) {
                reject();
            } else if (_.intersection(user.role, arguments).length) {
                resolve();
            } else {
                reject();
            }
        });
    }

    function checkUser(email, nickName, password)
    {
        var regexEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.exec(email);
        return new Promise(function (resolve, reject)
        {
            if (password && nickName && email && regexEmail) {
                resolve();
            } else {
                reject(applicationException.new(applicationException.VALIDATION_FAILURE));
            }
        });

    }

    module.exports = {
        ADMIN: 'admin',
        USER: 'user',
        isAdmin: isAdmin,
        isAuthenticated: isAuthenticated,
        checkAuthor: checkAuthor,
        checkRoles: checkRoles,
        checkUser: checkUser
    };
})();

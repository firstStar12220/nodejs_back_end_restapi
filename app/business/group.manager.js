(function ()
{
    'use strict';
    var security = require('./security'),
            GroupDAO = require('../DAO/GroupDAO');

    function create(context)
    {
        function search(query)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return GroupDAO.search(query, context.user.id);
            });
        }

        function createOrUpdate(group)
        {
            return security.isAuthenticated(context).then(function ()
            {
                if (group.authorId) {
                    return security.checkAuthor(group.authorId, context).then(function ()
                    {
                        return GroupDAO.createOrUpdate(group);
                    });
                } else {
                    group.authorId = context.user.id;
                    return GroupDAO.createOrUpdate(group);
                }
            });
        }

        function remove(groupId)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return GroupDAO.remove(context.user.id, groupId);
            });
        }

        function getEntity(groupId)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return GroupDAO.getEntity(context.user.id, groupId);
            });
        }

        return {
            getEntity: getEntity,
            search: search,
            createOrUpdate: createOrUpdate,
            remove: remove
        }
    }

    module.exports = {
        create: create
    }
})();

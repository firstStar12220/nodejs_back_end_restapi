(function ()
{
    'use strict';

    var UserDAO = require('../DAO/UserDAO'),
            SendgridManager = require('./sendgrid.manager'),
            PasswordDAO = require('../DAO/PasswordDAO'),
            ActivateTokenAndResetDAO = require('../DAO/ActivateTokenAndResetDAO'),
            TokenDAO = require('../DAO/TokenDAO'),
            Promise = require('bluebird'),
            _ = require('lodash'),
            moment = require('../services/dateHelper').moment,
            getDate = require('../services/dateHelper').get,
            security = require('./security'),
            sha1 = require('sha1');

    function hashPassword(password)
    {
        return sha1(password);
    }

    function register(user)
    {
        var data;
        return UserDAO.register(user).then(function (userFromDAO)
        {
            data = userFromDAO;
            return PasswordDAO.createOrUpdate(userFromDAO.id, hashPassword(user.password))
        }).then(function (password)
        {
            return ActivateTokenAndResetDAO.create(password.userId);
        }).then(function (token)
        {
            return SendgridManager.sendActivationEmail(token, data.email);
        });
    }

    function authenticate(email, password)
    {
        var data;
        return UserDAO.getActiveUserByEmail(email).then(function (user)
        {
            data = user;
            return PasswordDAO.authorize(user.id, hashPassword(password));
        }).then(function ()
        {
            return TokenDAO.create(data.id);
        });
    }

    function getUserByToken(token)
    {
        return TokenDAO.getByToken(token).then(function (tokenFromDB)
        {
            return UserDAO.getUserById(tokenFromDB.userId);
        });
    }

    function findAllUsersIsMidnight()
    {
        return new Promise(function (resolve)
        {
            var currentDate = getDate();
            UserDAO.getAllUser().then(function (users)
            {
                var arrayUser = [];
                _.forEach(users, function (user)
                {
                    if (0 === parseInt(moment(currentDate).tz(user.timezone).format('HH'), 10)) {
                        arrayUser.push(user);
                    }
                });
                resolve(arrayUser);
            });
        })
    }

    function resetPassword(email)
    {
        return UserDAO.getUserByEmail(email).then(function (user)
        {
            return ActivateTokenAndResetDAO.create(user.id)
        }).then(function (token)
        {
            return SendgridManager.resetPassword(email, token)
        });
    }

    function setPassword(token, password)
    {
        return ActivateTokenAndResetDAO.remove(token).then(function (userId)
        {
            return PasswordDAO.createOrUpdate(userId, hashPassword(password));
        });
    }

    function checkToken(token)
    {
        return ActivateTokenAndResetDAO.checkToken(token);
    }

    function activeAccount(token)
    {
        return ActivateTokenAndResetDAO.remove(token).then(function (userId)
        {
            return UserDAO.activeAccount(userId);
        });
    }

    function create(context)
    {
        function saveSettings(user)
        {
            return security.isAuthenticated(context).then(function ()
            {
                user.id = context.user.id;
                return UserDAO.saveSettings(user)
            });
        }

        return {
            saveSettings: saveSettings
        }
    }

    module.exports = {
        activeAccount: activeAccount,
        checkToken: checkToken,
        create: create,
        register: register,
        setPassword: setPassword,
        authenticate: authenticate,
        resetPassword: resetPassword,
        getUserByToken: getUserByToken,
        findAllUsersIsMidnight: findAllUsersIsMidnight
    };
})();

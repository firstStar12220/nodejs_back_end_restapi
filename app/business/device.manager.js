(function ()
{
    'use strict';
    var security = require('./security'),
            AlertDAO = require('../DAO/AlertDAO'),
            DeviceDAO = require('../DAO/DeviceDAO'),
            EmailDAO = require('../DAO/EmailDAO'),
            UserDAO = require('../DAO/UserDAO');

    function create(context)
    {
        function search(query, groupId)
        {
            return security.isAuthenticated(context).then(function ()
            {
                query.authorId = context.user.id;
                query.groupId = groupId;
                return DeviceDAO.search(query);
            });
        }

        function createOrUpdate(device)
        {
            return security.isAuthenticated(context).then(function ()
            {
                device.authorId = context.user.id;
                return DeviceDAO.createOrUpdate(device);
            });
        }

        function remove(deviceId)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return DeviceDAO.remove(deviceId, context.user.id).then(function ()
                {
                    return EmailDAO.remove(deviceId);
                });
            });
        }

        return {
            search: search,
            remove: remove,
            createOrUpdate: createOrUpdate
        }
    }

    module.exports = {
        create: create
    }
})();

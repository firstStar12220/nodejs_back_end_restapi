(function ()
{
    'use strict';

    var security = require('./security'),
            AlertDAO = require('../DAO/AlertDAO');

    function create(context)
    {
        function search()
        {
            return security.isAuthenticated(context).then(function ()
            {
                return AlertDAO.search()
            });
        }

        return {
            search: search
        }
    }

    module.exports = {
        create: create
    }
})();

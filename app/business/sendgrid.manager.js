(function ()
{
    'use strict';
    //TODO refactor in all  device.forwardEmail to  device.ticket

    var DeviceDAO = require('../DAO/DeviceDAO'),
            mongoose = require('mongoose'),
            ProductDAO = require('../DAO/ProductDAO'),
            EmailDAO = require('../DAO/EmailDAO'),
            UserDAO = require('../DAO/UserDAO'),
            AlertDAO = require('../DAO/AlertDAO'),
            Promise = require('bluebird'),
            config = require('../config/config'),
            sendgrid = require('sendgrid')(config.sendgrid.apiUser, config.sendgrid.apiKey),
            sendEmailViaSendgrid = Promise.promisify(sendgrid.send, sendgrid),
            jade = require('jade'),
            getDate = require('../services/dateHelper').get,
            moment = require('../services/dateHelper').moment,
            _ = require('lodash'),
            applicationException = require('../services/applicationException'),
            domain = config[process.env.PRODUCTION || 'test'].domain,
            pathToTemplate = config[process.env.PRODUCTION || 'test'].urlToTemplates;


    function replaceAll(find, replace, str)
    {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function parser(email)
    {
        function getOurEmail()
        {
            for (var i = 0; i < email.to.length; i++) {
                if (-1 !== email.to[i].trim().indexOf(config.email)) {
                    return i;
                }
            }
        }

        return new Promise(function (resolve, reject)
        {
            email.text = email.text ? email.text[0] : '';
            email.headers = email.headers ? email.headers[0] : '';
            email.attachments = email.attachments ? parseInt(email.attachments[0], 10) : 0;
            email.dkim = email.dkim ? email.dkim[0] : null;
            email.from = email.from ? email.from[0] : null;
            email.senderIp = email.sender_ip ? email.sender_ip[0] : null;
            email.envelope = email.envelope ? JSON.parse(email.envelope[0]) : '';
            if (email.envelope) {
                email.envelope.to = email.envelope.to[0];
            }
            email.charsets = email.charsets ? JSON.parse(email.charsets[0]) : '';
            email.subject = email.subject ? email.subject[0] : '';
            if (email.html) {
                email.html = email.html.join('')
            }
            email.to = email.to[0].split(',');
            var regex = /([a-z0-9]*)@/i, execRegex = regex.exec(email.to[getOurEmail()]);
            if (execRegex) {
                email.to = execRegex[1] + config.email;
                resolve(email);
            } else {
                reject();
            }

        });
    }

    function sendEmailAfterGetEmailWithKeyword(user, email)
    {
        var emailToSend = new sendgrid.Email();
        emailToSend.setTos(user.alertEmail || user.email);
        emailToSend.setHtml(email.html || email.text);
        emailToSend.setText(email.text || email.html);
        emailToSend.setFrom(config.sendgrid.setFrom);
        emailToSend.setSubject(email.subject);
        return sendEmailViaSendgrid(emailToSend);
    }


    function searchExWordSubject(product, emailSubject)
    {
        for (var i = 0; i < product.results.exemptSubject.length; i++) {
            if (-1 < emailSubject.indexOf(product.results.exemptSubject[i])) {
                emailSubject = replaceAll(product.results.exemptSubject[i], '', emailSubject);
            }
        }
        return emailSubject;
    }

    function searchExWordBody(product, emailBody)
    {
        for (var j = 0; j < product.results.exemptBody.length; j++) {
            if (-1 < emailBody.indexOf(product.results.exemptBody[j])) {
                emailBody = replaceAll(product.results.exemptBody[j], '', emailBody);
            }
        }
        return emailBody
    }

    function searchKeywords(product, emailSubject, emailBody, keywordSub, parsEmail, keywordBody)
    {
        for (var k = 0; k < product.results.keywordsSubject.length; k++) {
            if (-1 < emailSubject.indexOf(product.results.keywordsSubject[k])) {
                parsEmail.keywordSubject = product.results.keywordsSubject[k];
                keywordSub = true;
            }
        }
        if (!keywordSub) {
            for (var l = 0; l < product.results.keywordsBody.length; l++) {
                if (-1 < emailBody.indexOf(product.results.keywordsBody[l])) {
                    parsEmail.keywordBody = product.results.keywordsBody[l];
                    keywordBody = true;
                }
            }
        }
        return {keywordSub: keywordSub, keywordBody: keywordBody};
    }

    function isDeviceAlreadyMarkedAsHumanInteractionRequired(device)
    {
        return 'detected' !== device.status;
    }

    function checkEmail(email)
    {
        var exSubject, exBody, keywordSub, keywordBody, emailPars, deviceFromDAO, emailBody, emailSubject;
        return parser(email).then(function (parsEmail)
        {
            emailPars = parsEmail;
            emailBody = parsEmail.text;
            emailSubject = parsEmail.subject;
            parsEmail.to = parsEmail.to.trim();
            return DeviceDAO.getDeviceByEmail(parsEmail.to);
        }).then(function (device)
        {
            if (!device) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            emailPars.body = emailPars.text;
            emailPars.userId = device.authorId;
            emailPars.deviceId = device.id;
            deviceFromDAO = device;
            return ProductDAO.getEntity(device.productId);
        }).then(function (product)
        {
            emailSubject = searchExWordSubject(product, emailSubject, exSubject);
            emailBody = searchExWordBody(product, emailBody, exBody);
            var data = searchKeywords(product, emailSubject, emailBody, keywordSub, emailPars, keywordBody);
            keywordSub = data.keywordSub;
            keywordBody = data.keywordBody;

            if (keywordBody || keywordSub) {
                emailPars.status = !!keywordBody || !!keywordSub;
            }
            return EmailDAO.createOrUpdate(emailPars);
        }).then(function ()
        {
            return UserDAO.getUserById(deviceFromDAO.authorId);
        }).then(function (user)
        {
            var humanInteractionRequired = email.status;
            //status 'detected' in device is when found keyword in email
            if (humanInteractionRequired && deviceFromDAO.forwardEmail) {
                if (user.detected) {
                    if (isDeviceAlreadyMarkedAsHumanInteractionRequired(deviceFromDAO)) {
                        sendEmailAfterGetEmailWithKeyword(user, emailPars);
                    }
                } else {
                    sendEmailAfterGetEmailWithKeyword(user, emailPars);
                }
            }
            deviceFromDAO.status = humanInteractionRequired ? 'detected' : 'notDetected';
            deviceFromDAO.lastEmailReceive = getDate();
            return DeviceDAO.createOrUpdate(deviceFromDAO);
        }).catch(function (error)
        {
            console.error('checkEmail');
            console.error(error);
        });
    }

    function checkWhenSentReports(user, dayOfWeek)
    {
        return ('7 days a week' === user.alert ) ||
                ('on mondays' === user.alert && 1 === dayOfWeek ) ||
                ('on tuesdays' === user.alert && 2 === dayOfWeek ) ||
                ('on wednesdays' === user.alert && 3 === dayOfWeek) ||
                ('on thursdays' === user.alert && 4 === dayOfWeek) ||
                ('on fridays' === user.alert && 5 === dayOfWeek) ||
                ('on Saturdays' === user.alert && 6 === dayOfWeek) ||
                ('on Sundays' === user.alert && 7 === dayOfWeek) ||
                ('bi weekly' === user.alert && 1 === dayOfWeek) ||
                ('mon-friday' === user.alert && (2 <= dayOfWeek && 6 >= dayOfWeek)) ||
                ('Monthly' === user.alert && 1 === parseInt(moment().tz(user.timezone).format('D'), 10));
    }

    //This handels the script time when it will check if an email is recived within an expected time. Note below the 23 is 11:00pm
    function findAllUsersIsMidnight()
    {
        return new Promise(function (resolve)
        {
            var currentDate = getDate();
            UserDAO.getAllUser().then(function (users)
            {
                var arrayUser = [];
                _.forEach(users, function (user)
                {
                    if (user.timezone && 0 === parseInt(moment(currentDate).tz(user.timezone).format('HH'), 10)) {
                        arrayUser.push(user);
                    }
                });
                resolve(arrayUser);
            });
        });
    }

    function getUsersToSendReports()
    {
        return new Promise(function (resolve)
        {
            var currentDate = getDate();
            UserDAO.getAllUser().then(function (users)
            {
                var arrayUser = [];
                _.forEach(users, function (user)
                {
                    if (user.timezone && config.hourWhenSendReports === parseInt(moment(currentDate).tz(user.timezone).format('HH'), 10)) {
                        arrayUser.push(user);
                    }
                });
                resolve(arrayUser);
            });
        });
    }

    function sendReports()
    {
        var promises;
        return getUsersToSendReports().then(function (users)
        {
            var currentDate = getDate();
            promises = _.map(users, function (user)
            {
                var dayOfWeek = parseInt(moment(currentDate).tz(user.timezone).format('E'), 10);
                if (checkWhenSentReports(user, dayOfWeek)) {
                    return DeviceDAO.reportDevice(user.id, user.timezone).then(function (devices)
                    {
                        devices.title = 'Reports';
                        var compile = jade.compileFile(pathToTemplate + 'reports.tpl.jade', {pretty: true});
                        var htmlOut = compile(devices);
                        var emailToSend = new sendgrid.Email();
                        emailToSend.setTos(user.reportsEmail || user.email);
                        emailToSend.setHtml(htmlOut);
                        emailToSend.setFrom(config.sendgrid.setFrom);
                        emailToSend.setSubject('Reports');
                        return sendEmailViaSendgrid(emailToSend);
                    });
                }
            });
            return Promise.all(promises);
        });
    }

    function sendEmailDelayedDevice(user, device)
    {
        return require('bluebird').resolve();
//    var email = new sendgrid.Email();
//    email.setTos(user.alertEmail || user.email);
//    email.setFrom(config.sendgrid.setFrom);
//    email.setSubject('Alert!!!');
//    email.setText('Alert Centric did not receive an email within expected time for device' + device.name + ' under group ' + device.group);
//    email.setHtml('Alert Centric did not receive an email within expected time for device <b>' +
//        device.name +
//        '</b> under group <b>' +
//        device.group +
//        '</b>');
//    return sendEmailViaSendgrid(email);
    }


    function sendEmail(email, userEmail)
    {
        var sendEmail = new sendgrid.Email();
        sendEmail.setFrom(userEmail);
        sendEmail.setTos(email.sendEmail);
        sendEmail.setSubject(email.subject);
        sendEmail.setText(email.body);
        sendEmail.setHtml(email.body);
        return sendEmailViaSendgrid(sendEmail);
    }

    function resetPassword(userEmail, token)
    {
        var email = new sendgrid.Email();
        var compile = jade.compileFile(pathToTemplate + 'resetPassword.tpl.jade', {pretty: true});
        var htmlOut = compile({url: domain + 'passwordreset/' + token});
        email.setFrom(config.sendgrid.setFrom);
        email.setTos(userEmail);
        email.setSubject('Reset password');
        email.setHtml(htmlOut);
        return sendEmailViaSendgrid(email);
    }

    function sendActivationEmail(token, userEmail)
    {
        var email = new sendgrid.Email();
        email.setTos(userEmail || 'dave@alertcentric.com');
        email.setFrom(config.sendgrid.setFrom);
        email.setSubject('Active account');
        var compile = jade.compileFile(pathToTemplate + 'activeAccount.tpl.jade', {pretty: true});
        var htmlOut = compile({url: domain + 'active/' + token});
        email.setHtml(htmlOut);
        return sendEmailViaSendgrid(email);
    }


    function sendContact(sendEmail, userEmail)
    {
        var email = new sendgrid.Email();
        email.setTos('help@alertcentric.com');
        email.setFrom(userEmail);
        email.setSubject('Please add vendor or product');
        var compile = jade.compileFile(pathToTemplate + 'addProduct.tpl.jade', {pretty: true});
        var htmlOut = compile(sendEmail);
        email.setHtml(htmlOut);
        return sendEmailViaSendgrid(email);
    }

    function getDevicesUser(user)
    {
        var todayMidnight = moment(getDate()).tz(user.timezone).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).valueOf();
        var oneDay = 86400000;
        var yesterdayMidnight = todayMidnight - oneDay;
        var tomorrowNight = todayMidnight + oneDay;

        function checkAlert(device, lastEmailReceiveDevice, dayOfWeek)
        {
            var checkMidnights = !((yesterdayMidnight < lastEmailReceiveDevice) && (tomorrowNight > lastEmailReceiveDevice));


            return !!('7 days a week' === device.alert && checkMidnights) ||
                    ('on mondays' === device.alert && 2 === dayOfWeek && checkMidnights) ||
                    ('on tuesdays' === device.alert && 3 === dayOfWeek && checkMidnights) ||
                    ('on wednesdays' === device.alert && 4 === dayOfWeek && checkMidnights ) ||
                    ('on thursdays' === device.alert && 5 === dayOfWeek && checkMidnights ) ||
                    ('on fridays' === device.alert && 6 === dayOfWeek && checkMidnights ) ||
                    ('on Saturdays' === device.alert && 7 === dayOfWeek && checkMidnights) ||
                    ('on Sundays' === device.alert && 1 === dayOfWeek && checkMidnights ) ||
                    ('bi weekly' === device.alert && (todayMidnight - ( 15 * oneDay) > lastEmailReceiveDevice)) ||
                    ('mon-friday' === device.alert && (2 <= dayOfWeek && 6 >= dayOfWeek) && checkMidnights ) ||
                    ('Monthly' === device.alert && (todayMidnight - (31 * oneDay) > lastEmailReceiveDevice));

        }

        var promises;
        return DeviceDAO.getAllDevicesWithDetails(user.id).then(function (devicesWithEmail)
        {
            var sendEmail = false;
            var currentDate = getDate();
            promises = _.map(devicesWithEmail, function (device)
            {
                sendEmail = false;
                var dayOfWeek = parseInt(moment(currentDate).tz(user.timezone).format('E'), 10);
                if ('enabled' === device.forwardEmail) {
                    if (!device.lastEmailReceive) {
                        if (device.createDate) {
                            sendEmail = checkAlert(device, device.createDate, dayOfWeek);
                        } else {
                            var createDate = new mongoose.Types.ObjectId(device.id).getTimestamp();
                            createDate = moment(createDate).tz(user.timezone).set({
                                hour: 0,
                                minute: 0,
                                second: 0,
                                millisecond: 0
                            }).valueOf();
                            sendEmail = checkAlert(device, createDate, dayOfWeek);
                        }
                    } else {
                        sendEmail = checkAlert(device, device.lastEmailReceive, dayOfWeek);
                    }
                }
                if (sendEmail) {
                    return sendEmailDelayedDevice(user, device).then(function ()
                    {
                        return DeviceDAO.setDelay(device.id);
                    });
                } else {
                    return Promise.resolve();
                }
            });
            return Promise.all(promises);
        });
    }

    function findDelayDevices()
    {
        var promises;
        return AlertDAO.search().then(function (alerts)
        {
            return findAllUsersIsMidnight().then(function (filterUsers)
            {
                promises = _.map(filterUsers, function (user)
                {
                    return getDevicesUser(user, alerts);
                });
                return Promise.all(promises);
            });
        });
    }

    setInterval(function ()
    {
        findDelayDevices().then(sendReports);
    }, 3600000);

    module.exports = {
        sendEmail: sendEmail,
        checkEmail: checkEmail,
        sendReports: sendReports,
        resetPassword: resetPassword,
        sendEmailDelayedDevice: sendEmailDelayedDevice,
        sendActivationEmail: sendActivationEmail,
        sendContact: sendContact,
        findDelayDevices: findDelayDevices
    };
})();

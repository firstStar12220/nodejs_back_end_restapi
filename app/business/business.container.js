(function ()
{
    'use strict';

    var clientManager = require('./group.manager'),
            deviceManager = require('./device.manager'),
            vendorManager = require('./vendor.manager'),
            productManager = require('./product.manager'),
            alertManager = require('./alert.manager'),
            emailManager = require('./email.manger'),
            userManager = require('./user.manager');

    function getContext(request)
    {
        return {user: request.user};
    }

    function getter(manager)
    {
        return function (request)
        {
            return manager.create(getContext(request));
        };
    }


    module.exports = {
        getUserManager: getter(userManager),
        getGroupManager: getter(clientManager),
        getDeviceManager: getter(deviceManager),
        getVendorManager: getter(vendorManager),
        getProductManager: getter(productManager),
        getAlertManager: getter(alertManager),
        getEmailManager: getter(emailManager)
    };
})();

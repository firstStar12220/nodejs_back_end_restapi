(function ()
{
    'use strict';

    var security = require('./security'),
            ProductDAO = require('../DAO/ProductDAO'),
            VendorDAO = require('../DAO/VendorDAO');

    function create(context)
    {
        function search(vendorId)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return ProductDAO.search(vendorId);
            });
        }

        function createOrUpdate(product)
        {
            return security.isAdmin(context).then(function ()
            {
                return VendorDAO.getEntity(product.vendorId).then(function ()
                {
                    return ProductDAO.createOrUpdate(product);
                });
            });
        }

        function getEntity(productId)
        {
            return security.isAdmin(context).then(function ()
            {
                return ProductDAO.getEntity(productId);
            });
        }

        function addWord(productId, word)
        {
            return security.isAdmin(context).then(function ()
            {
                return ProductDAO.addWord(productId, word);
            });
        }

        function remove(id)
        {
            return security.isAdmin(context).then(function ()
            {
                return ProductDAO.remove(id);
            });
        }

        return {
            remove: remove,
            addWord: addWord,
            search: search,
            getEntity: getEntity,
            createOrUpdate: createOrUpdate
        }
    }

    module.exports = {
        create: create
    }
})();

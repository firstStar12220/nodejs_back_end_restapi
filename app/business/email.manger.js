(function ()
{
    'use strict';
    var security = require('./security'),
            config = require('../config/config'),
            sendgridManager = require('./sendgrid.manager'),
            shortid = require('shortid'),
            DeviceDAO = require('../DAO/DeviceDAO'),
            EmailDAO = require('../DAO/EmailDAO'),
            rString = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    function randomString(length, chars)
    {
        var result = '';
        for (var i = length; i > 0; --i) {
            result += chars[Math.round(Math.random() * (chars.length - 1))];
        }
        return result;
    }


    function create(context)
    {
        function getEmail()
        {
            return security.isAuthenticated(context).then(function ()
            {
                var email = randomString(20, rString) + config.email;
                return DeviceDAO.getDeviceByEmail(email).then(function (result)
                {
                    if (!result) {
                        return {results: email};
                    } else {
                        getEmail();
                    }
                });
            });
        }

        function search(deviceId, query)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return EmailDAO.search(deviceId, query, context.user.id);
            });
        }

        function sendEmail(email)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return sendgridManager.sendEmail(email, context.user.email);
            });
        }

        function sendContact(email)
        {
            return security.isAuthenticated(context).then(function ()
            {
                return sendgridManager.sendContact(email, context.user.email);
            });
        }

        return {
            getEmail: getEmail,
            sendContact: sendContact,
            search: search,
            sendEmail: sendEmail
        }
    }

    module.exports = {
        create: create
    }
})();

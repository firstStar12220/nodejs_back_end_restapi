(function ()
{
    'use strict';
    module.exports = {
        database: {resource: process.env.MONGOLAB_URI || 'mongodb://localhost/backend'},
        //here you can set when send daily reports in 24h
        hourWhenSendReports: 7,
        email: '@mail.alertcentric.com',
        sendgrid: {
            apiUser: process.env.sendGridapiKey || 'asdlw',
            apiKey: process.env.sendGridUserKey || '1qazxsw@!',
            setFrom: 'noreply@mail.alertcentric.com'
        },
        production: {
            urlToTemplates: './app/templates/',
            domain: 'http://asdlw-fronted.herokuapp.com/#/'
        },
        test: {
            urlToTemplates: '../app/templates/',
            domain: 'http://localhost:9000/#/'
        }

    };
})();

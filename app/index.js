(function ()
{
    'use strict';

    var express = require('express');
    var morgan = require('morgan');
    var mongoose = require('mongoose');
    var bodyParser = require('body-parser');
    var configDB = require('./config/config').database;
    var app = express();
    app.use(express.static(__dirname + '/'));
    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());

    mongoose.connect(configDB.resource, function (error)
    {
        if (error) {
            console.log(error);
        }
    });
    // If the Node process ends, close the Mongoose connection
    process.on('SIGINT', function ()
    {
        mongoose.connection.close(function ()
        {
            console.log('Mongoose default connection disconnected through app termination');
            process.exit(0);
        });
    });
    //console.log(mongoose.Types.ObjectId());
    //require('./../seed/seed')('./../seed/seedData.json');
    require('./REST/routes.js')(app);
    app.listen(process.env.PORT || 3000, function ()
    {
        console.log('Server is ready!.');
    });

    module.exports = app;
})();

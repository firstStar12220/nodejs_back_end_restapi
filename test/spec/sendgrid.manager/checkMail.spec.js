describe('checkEmail', function ()
{
    'use strict';

    var proxyquire = require('proxyquire');
    var testHelper = require('../../testHelper'),
            EmailModel = testHelper.getEmailModel(),
            DeviceModel = testHelper.getDeviceModel(),
            sinon = require('sinon'),
            assert = require('assert');
    var setTosMock, setHtmlMock, setTextMock, setFromMock, setSubjectMock, EmailMock, sendMock, checkEmail, sendgridMock;
    beforeEach(function ()
    {
        sendgridMock = sinon.spy(function ()
        {
            return {
                send: sendMock,
                Email: EmailMock
            }
        });
        sendMock = sinon.spy(function (data, callback)
        {
            callback();
        });
        setTosMock = sinon.spy();
        setHtmlMock = sinon.spy();
        setTextMock = sinon.spy();
        setFromMock = sinon.spy();
        setSubjectMock = sinon.spy();
        EmailMock = sinon.spy(function ()
        {
            return {
                setTos: setTosMock,
                setHtml: setHtmlMock,
                setText: setTextMock,
                setFrom: setFromMock,
                setSubject: setSubjectMock
            }
        });

        checkEmail = proxyquire('../../../app/business/sendgrid.manager', {
            sendgrid: sendgridMock
        }).checkEmail;

        return testHelper.openDBConnection()
                .then(testHelper.readFileToSeed)
                .then(testHelper.seedUsers)
                .then(testHelper.seedAlert)
                .then(testHelper.seedGroups)
                .then(testHelper.seedPassword)
                .then(testHelper.seedProduct)
                .then(testHelper.seedVendor)
                .then(testHelper.seedDevices);
    });
    afterEach(function (done)
    {
        testHelper.closeDBConnection(done);
    });

    describe('when user check forward email in settings', function ()
    {
        describe('and send email to new device', function ()
        {
            describe('when email contains only keywords', function ()
            {
                describe('in subject', function ()
                {
                    describe('when keyword is only one word', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to testing check mailing'],
                                subject: ['this is simple subject'],
                                to: ['test@test.com', '4J0vWZmq@mail.alertcentric.com']
                            })
                        });

                        it('should add email to collection with status set to true', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this is simple subject'}).then(function (results)
                            {
                                assert.strictEqual(results.status, true);
                            });

                        });
                        it('should change device status to \'detected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'detected');
                            });
                        });
                        it('should should send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 1);
                            assert.equal(setTosMock.args[0], 'test@example.com');
                            assert.equal(setSubjectMock.args[0], 'this is simple subject');
                            assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                        });
                    });
                    describe('when keyword is phrase', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to testing check mailing'],
                                subject: ['this email test phrase subject'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should found keyword and save email with status set to true', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this email test phrase subject'}).then(function (results)
                            {
                                assert.strictEqual(results.status, true);
                            });
                        });
                        it('should change device status to \'detected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'detected');
                            });
                        });
                        it('should should send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 1);
                            assert.equal(setTosMock.args[0], 'test@example.com');
                            assert.equal(setSubjectMock.args[0], 'this email test phrase subject');
                            assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                        });
                    });
                });
                describe('in body', function ()
                {
                    describe('when keyword is only one word', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to testing check mailing'],
                                subject: ['this is simple'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should add email to collection with status set to true', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this is simple'}).then(function (results)
                            {
                                assert.strictEqual(results.status, true);
                            });
                        });
                        it('should change device status to \'detected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'detected');
                            });
                        });
                        it('should should send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 1);
                            assert.equal(setTosMock.args[0], 'test@example.com');
                            assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                        });
                    });
                    describe('when keyword is phrase', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['some text Failing: 0 another text'],
                                subject: ['thi email test phrase subject'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should found keyword and save email with status set to true', function ()
                        {

                            return EmailModel.findOneQ({subject: 'thi email test phrase subject'}).then(function (results)
                            {
                                assert.strictEqual(results.status, true);
                            });
                        });
                        it('should change device status to \'detected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'detected');
                            });
                        });
                        it('should should send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 1);
                            assert.equal(setTosMock.args[0], 'test@example.com');
                            assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                        });
                    });
                });
            });
            describe('when email contains only exempt words', function ()
            {
                describe('in subject', function ()
                {
                    describe('when exempt word is only one word', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to testing check'],
                                subject: ['this is testing exempt'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should add email to device and change status status to false', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this is testing exempt'}).then(function (results)
                            {
                                assert.strictEqual(results.status, false);
                            });
                        });
                        it('should change status device to \'notDetected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'notDetected');
                            });
                        });
                        it('should should don\'t send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 0);
                            assert.strictEqual(setTosMock.callCount, 0);
                            assert.strictEqual(setFromMock.callCount, 0);
                        });
                    });
                    describe('when exempt word is phrase', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to testing check'],
                                subject: ['this is testing exempt phrase exempt'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should add email to device and don\'t with status set to false', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this is testing exempt phrase exempt'}).then(function (results)
                            {
                                assert.strictEqual(results.status, false);
                            });
                        });
                        it('should change status device to \'notDetected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'notDetected');
                            });
                        });
                        it('should should don\'t send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 0);
                            assert.strictEqual(setTosMock.callCount, 0);
                            assert.strictEqual(setFromMock.callCount, 0);
                        });
                    });
                });
                describe('in body', function ()
                {
                    describe('when exempt word is only one word', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test body to testing body check'],
                                subject: ['this is testing'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should add email to device and don\'t with status set to false', function ()
                        {
                            return EmailModel.findOneQ({subject: 'this is testing'}).then(function (results)
                            {
                                assert.strictEqual(results.status, false);
                            });
                        });
                        it('should change status in device to \'notDetected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'notDetected');
                            });
                        });
                        it('should should don\'t send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 0);
                            assert.strictEqual(setTosMock.callCount, 0);
                            assert.strictEqual(setFromMock.callCount, 0);
                        });
                    });
                    describe('when exempt word is phrase', function ()
                    {
                        beforeEach(function ()
                        {
                            return checkEmail({
                                text: ['simple test to phrase exempt body check'],
                                subject: ['testing'],
                                to: ['4J0vWZmq@mail.alertcentric.com']
                            });
                        });
                        it('should add email to device and don\'t with status set to false', function ()
                        {
                            return EmailModel.findOneQ({subject: 'testing'}).then(function (results)
                            {
                                assert.strictEqual(results.status, false);
                            });
                        });
                        it('should change interaction device to \'notDetected\'', function ()
                        {
                            return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                            {
                                assert.strictEqual(results.status, 'notDetected');
                            });
                        });
                        it('should should don\'t send email to user', function ()
                        {
                            assert.strictEqual(sendMock.callCount, 0);
                            assert.strictEqual(setTosMock.callCount, 0);
                            assert.strictEqual(setFromMock.callCount, 0);
                        });
                    });
                });
            });
            //mail.alertcentric.com	http://asdlw-backend.herokuapp.com/api/sendgrid
            describe('when email contain keyword and exempt word', function ()
            {
                describe('in subject', function ()
                {
                    beforeEach(function ()
                    {
                        return checkEmail({
                            text: ['some text another text'],
                            subject: ['subject and exempt testing'],
                            to: ['4J0vWZmq@mail.alertcentric.com']
                        });
                    });
                    it('should found keyword and save email with status set to true', function ()
                    {

                        return EmailModel.findOneQ({subject: 'subject and exempt testing'}).then(function (results)
                        {
                            assert.strictEqual(results.status, true);
                        });
                    });
                    it('should change device status to \'detected\'', function ()
                    {
                        return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                        {
                            assert.strictEqual(results.status, 'detected');
                        });
                    });
                    it('should should send email to user', function ()
                    {
                        assert.strictEqual(sendMock.callCount, 1);
                        assert.strictEqual(setTosMock.callCount, 1);
                        assert.strictEqual(setFromMock.callCount, 1);
                    });
                });
                describe('in body', function ()
                {
                    beforeEach(function ()
                    {
                        return checkEmail({
                            text: ['body text Failing: 1 another text Failing: 0'],
                            subject: ['some test'],
                            to: ['4J0vWZmq@mail.alertcentric.com']
                        });
                    });
                    it('should found keyword and save email with status set to true', function ()
                    {
                        return EmailModel.findOneQ({subject: 'some test'}).then(function (results)
                        {
                            assert.strictEqual(results.status, true);
                        });
                    });
                    it('should change device status to \'detected\'', function ()
                    {
                        return DeviceModel.findOneQ({email: '4J0vWZmq@mail.alertcentric.com'}).then(function (results)
                        {
                            assert.strictEqual(results.status, 'detected');
                        });
                    });
                    it('should should send email to user', function ()
                    {
                        assert.strictEqual(sendMock.callCount, 1);
                        assert.strictEqual(setTosMock.callCount, 1);
                        assert.strictEqual(setFromMock.callCount, 1);
                    });
                });
            });
            describe('when email someone send email without body', function ()
            {
                beforeEach(function ()
                {
                    return checkEmail({
                        subject: ['when email someone subject send email without body'],
                        to: ['4J0vWZmq@mail.alertcentric.com']
                    });
                });
                it('should add email to collection', function ()
                {
                    return EmailModel.countQ({subject: 'when email someone subject send email without body'}).then(function (count)
                    {
                        assert.strictEqual(count, 1);
                    });
                });
                it('should should send email to user', function ()
                {
                    assert.strictEqual(sendMock.callCount, 1);
                    assert.strictEqual(setTosMock.callCount, 1);
                    assert.strictEqual(setFromMock.callCount, 1);
                });
            });
            describe('when someone send email without subject', function ()
            {
                beforeEach(function ()
                {
                    return checkEmail({
                        text: ['when email mailing someone send email without subject'],
                        to: ['4J0vWZmq@mail.alertcentric.com']
                    });
                });
                it('should add email to collection', function ()
                {
                    return EmailModel.countQ({body: 'when email mailing someone send email without subject'}).then(function (count)
                    {
                        assert.strictEqual(count, 1);
                    });
                });
                it('should should send email to user', function ()
                {
                    assert.strictEqual(sendMock.callCount, 1);
                    assert.strictEqual(setTosMock.callCount, 1);
                    assert.strictEqual(setFromMock.callCount, 1);
                });
            });
        });
        describe('when device have status delay', function ()
        {
            describe('and contain keyword', function ()
            {
                beforeEach(function ()
                {
                    return checkEmail({
                        text: ['simple body not contain keywords'],
                        subject: ['test to test for testing device ony check device'],
                        to: ['forTest@mail.alertcentric.com']
                    })
                });
                it('should add email to collection with status set to true', function ()
                {
                    return EmailModel.findOneQ({subject: 'test to test for testing device ony check device'}).then(function (results)
                    {
                        assert.strictEqual(results.status, true);
                    });
                });
                it('should change status device to \'detected\'', function ()
                {
                    return DeviceModel.findOneQ({email: 'forTest@mail.alertcentric.com'}).then(function (results)
                    {
                        assert.strictEqual(results.status, 'detected');
                    });
                });
                it('should send email to user', function ()
                {
                    assert.strictEqual(sendMock.callCount, 1);
                    assert.equal(setTosMock.args[0], 'example@example.com')
                });
            });
        });
        describe('when status have status not detected', function ()
        {
            beforeEach(function ()
            {
                return checkEmail({
                    text: ['simple test to testing'],
                    subject: ['simple text to test'],
                    to: ['notDetected@mail.alertcentric.com']
                })
            });
            it('should add email to collection with status to true', function ()
            {
                return EmailModel.findOneQ({subject: 'simple text to test'}).then(function (results)
                {
                    assert.equal(results.status, true);
                });
            });
            it('should change status device to \'detected\'', function ()
            {
                return DeviceModel.findOneQ({email: 'notDetected@mail.alertcentric.com'}).then(function (results)
                {
                    assert.strictEqual(results.status, 'detected');
                });
            });
            it('should send email to user', function ()
            {
                assert.strictEqual(sendMock.callCount, 1)
            });
        });
        describe('when status have status detected', function ()
        {
            beforeEach(function ()
            {
                return checkEmail({
                    text: ['simple test to testing'],
                    subject: ['simple text to test'],
                    to: ['detected@mail.alertcentric.com']
                })
            });
            it('should add email to collection with status to true', function ()
            {
                return EmailModel.findOneQ({subject: 'simple text to test'}).then(function (results)
                {
                    assert.equal(results.status, true);
                });
            });
            it('should change status device to \'detected\'', function ()
            {
                return DeviceModel.findOneQ({email: 'detected@mail.alertcentric.com'}).then(function (results)
                {
                    assert.strictEqual(results.status, 'detected');
                });
            });
            it('should send email to user', function ()
            {
                assert.strictEqual(sendMock.callCount, 0)
            });
        });
    });
    describe('when user not check detected in settings', function ()
    {
        describe('and email contain keywords', function ()
        {
            beforeEach(function ()
            {
                return checkEmail({
                    text: ['test'],
                    subject: ['test'],
                    to: ['VJy7MWm9@mail.alertcentric.com']
                }).then(function ()
                {
                    return checkEmail({
                        text: ['test'],
                        subject: ['test'],
                        to: ['VJy7MWm9@mail.alertcentric.com']
                    })
                })
            });
            it('should send email always when find keyword', function ()
            {
                assert.strictEqual(sendMock.callCount, 2);
                assert.strictEqual(setTosMock.callCount, 2);
                assert.equal(setTosMock.args[0], 'simple.test@gmail.com', 'Function send email to other user or not sent email')
            });
            it('should change status device to \'detected\'', function ()
            {
                return DeviceModel.findOneQ({email: 'VJy7MWm9@mail.alertcentric.com'}).then(function (results)
                {
                    assert.strictEqual(results.status, 'detected');
                });
            });
            it('should add all emails to collection', function ()
            {
                return EmailModel.countQ({subject: 'test'}).then(function (count)
                {
                    assert.strictEqual(count, 2);
                });
            });
        });
        describe('and email not contain keywords', function ()
        {
            beforeEach(function ()
            {
                return checkEmail({
                    text: ['simple'],
                    subject: ['some gone wrong:P'],
                    to: ['VJy7MWm9@mail.alertcentric.com']
                }).then(function ()
                {
                    return checkEmail({
                        text: ['simple'],
                        subject: ['some gone wrong:P'],
                        to: ['VJy7MWm9@mail.alertcentric.com']
                    })
                })
            });
            it('should not\'t send email', function ()
            {
                assert.strictEqual(sendMock.callCount, 0);
                assert.strictEqual(setTosMock.callCount, 0);
            });
            it('should change status device to \'notDetected\'', function ()
            {
                return DeviceModel.findOneQ({email: 'VJy7MWm9@mail.alertcentric.com'}).then(function (results)
                {
                    assert.strictEqual(results.status, 'notDetected');
                });
            });
            it('should add all emails to collection', function ()
            {
                return EmailModel.countQ({subject: 'some gone wrong:P'}).then(function (count)
                {
                    assert.strictEqual(count, 2);
                });
            });
        });
    });
    describe('when send email to not exist device', function ()
    {
        beforeEach(function ()
        {
            return checkEmail({
                text: ['simple test to testing check mailing'],
                subject: ['this is simple subject'],
                to: ['simpletestTOtest@mail.alertcentric.com']
            })
        });
        it('should not add email to collection', function ()
        {
            return EmailModel.findOneQ({subject: 'this is simple subject'}).then(function (results)
            {
                assert.strictEqual(results, null)
            });
        });
    });
});

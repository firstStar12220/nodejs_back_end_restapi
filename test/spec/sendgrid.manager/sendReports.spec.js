describe('sendReports', function ()
{
    'use strict';

    var proxyquire = require('proxyquire');
    var testHelper = require('../../testHelper'),
            sinon = require('sinon'),
            assert = require('assert'),
            setDate = require('../../../app/services/dateHelper').set,
            pwd = require('process-pwd');
    var setTosMock, setHtmlMock, setTextMock, setFromMock, setSubjectMock, EmailMock, sendMock, sendReports, sendgridMock, compileMock, compileFileMock;
    beforeEach(function ()
    {
        sendgridMock = sinon.spy(function ()
        {
            return {
                send: sendMock,
                Email: EmailMock
            }
        });
        sendMock = sinon.spy(function (data, callback)
        {
            callback();
        });
        setTosMock = sinon.spy();
        setHtmlMock = sinon.spy();
        setTextMock = sinon.spy();
        setFromMock = sinon.spy();
        setSubjectMock = sinon.spy();
        EmailMock = sinon.spy(function ()
        {
            return {
                setTos: setTosMock,
                setHtml: setHtmlMock,
                setText: setTextMock,
                setFrom: setFromMock,
                setSubject: setSubjectMock
            }
        });
        compileMock = sinon.spy(function ()
        {
            return 'Mock html function in jade';
        });
        compileFileMock = sinon.spy(function ()
        {

            return compileMock;
        });
        sendReports = proxyquire('../../../app/business/sendgrid.manager', {
            sendgrid: sendgridMock,
            '../config/config': {test: {urlToTemplates: './app/templates/'}},
            jade: {
                compileFile: compileFileMock
            }
        }).sendReports;

        return testHelper.openDBConnection()
                .then(testHelper.readFileToSeed)
                .then(testHelper.seedUsers)
                .then(testHelper.seedAlert)
                .then(testHelper.seedGroups)
                .then(testHelper.seedPassword)
                .then(testHelper.seedProduct)
                .then(testHelper.seedVendor)
                .then(testHelper.seedDevices);
    });
    afterEach(function (done)
    {
        testHelper.closeDBConnection(done)
    });
    describe('when in system not exist users where is midnight', function ()
    {
        beforeEach(function ()
        {
            setDate(8636562);
            return sendReports();
        });
        it('should not send email', function ()
        {
            assert.strictEqual(sendMock.callCount, 0);
        });
    });
    describe('when in system exist user where is midnight', function ()
    {
        describe('and users set send reports every day', function ()
        {
            describe('from UTC', function ()
            {
                beforeEach(function ()
                {
                    setDate(1438820000000);
                    return sendReports();
                });
                it('should sent report to the user', function ()
                {
                    assert.strictEqual(compileFileMock.callCount, 2);
                    assert.deepEqual(compileFileMock.args[0], ['./app/templates/reports.tpl.jade', {pretty: true}]);
                    var expectedToFirstUser = [
                        {
                            'delay': [],
                            'interactionRequired': [],
                            'notDetected': [
                                {
                                    'alert': 'on wednesdays',
                                    'alertId': '559faf465df5674977d51856',
                                    'authorId': '55c10cd038c8c81c3681df3a',
                                    'email': 'VJy7MWm9@mail.alertcentric.com',
                                    'forwardEmail': 'enabled',
                                    'group': 'Printers',
                                    'createDate': 8636562,
                                    'groupId': '55ba12c964b69a18085b3ec0',
                                    'id': {
                                        '_bsontype': 'ObjectID',
                                        'id': 'Uº\u0013\bd¶\u0018\b[>Â'
                                    },
                                    'name': 'HP Laserjet F31',
                                    'product': 'Printer',
                                    'productId': '559fb1e9bdda2d4e7afe4310',
                                    'status': 'other',
                                    'vendor': 'HP',
                                    'vendorId': '559faffb504387237880abce'
                                }
                            ],
                            'title': 'Reports'
                        }
                    ];
                    assert.deepEqual(compileMock.args[0], expectedToFirstUser);
                    var expectedToSecondUser = [
                        {
                            'delay': [],
                            'interactionRequired': [
                                {
                                    'alert': 'mon-friday',
                                    "lastEmailReceive": "4th Aug - 09:03pm",
                                    'alertId': '54ca236274cdf54054242511',
                                    'authorId': '55a5143a12add7c659fd2a08',
                                    'email': '4y3q-b79@mail.alertcentric.com',
                                    'forwardEmail': 'enabled',
                                    'group': 'Servers',
                                    'createDate': 8636562,
                                    'groupId': '55ba0abf64b69a18085b3ebd',
                                    'id': {
                                        '_bsontype': 'ObjectID',
                                        'id': 'Uº\u0012}d¶\u0018\b[>¿'
                                    },
                                    'name': 'Spectre W230',
                                    'product': 'Mobile',
                                    'productId': '559fb1e5cf1e6c3b7a015c6d',
                                    'status': 'detected',
                                    'vendor': 'Dell',
                                    'vendorId': '559fafe733320dfa770aa95b'
                                }
                            ],
                            'notDetected': [
                                {
                                    'alert': 'only upon failure',
                                    'alertId': '559fafbacbcd85de775ae79b',
                                    'authorId': '55a5143a12add7c659fd2a08',
                                    'email': '4J0vWZmq@mail.alertcentric.com',
                                    'createDate': 8636562,
                                    'forwardEmail': 'enabled',
                                    'group': 'Servers',
                                    'groupId': '55ba0abf64b69a18085b3ebd',
                                    'id': {
                                        '_bsontype': 'ObjectID',
                                        'id': 'Uº\u0012Sd¶\u0018\b[>¾'
                                    },
                                    'name': 'Blade X1',
                                    'product': 'Mock product',
                                    'productId': '54ca236274cdf54054242568',
                                    'status': 'other',
                                    'vendor': 'Vendor from backend',
                                    'vendorId': '54ca236274cdf54054242567'
                                }
                            ],
                            'title': 'Reports'
                        }
                    ];
                    assert.deepEqual(compileMock.args[1], expectedToSecondUser);
                    assert.deepEqual(setTosMock.args, [['report2@contact.com'], ['report@contact.com']]);
                    assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                    assert.equal(setFromMock.args[1], 'noreply@mail.alertcentric.com');
                    assert.equal(setSubjectMock.args[0], 'Reports');
                    assert.equal(setSubjectMock.args[1], 'Reports');
                    assert.equal(sendMock.callCount, 2);
                    assert.equal(setHtmlMock.args[0], 'Mock html function in jade');

                });
            });
            describe('from australia', function ()
            {
                beforeEach(function (done)
                {
                    setDate(1436000000 + 24 * 3600000);
                    sendReports().then(function ()
                    {
                        done()
                    });
                });
                it('should sent report to the user', function (done)
                {
                    assert.strictEqual(compileFileMock.callCount, 1);
                    assert.deepEqual(compileFileMock.args[0], ['./app/templates/reports.tpl.jade', {pretty: true}]);
                    var expectedToFirstUser = [
                        {
                            'delay': [],
                            'interactionRequired': [],
                            'notDetected': [
                                {
                                    'alert': '7 days a week',
                                    'alertId': '54ca236274cdf54054242512',
                                    'authorId': '55c31e85f1d2f3f73f41e72c',
                                    'email': 'NygWGbmq@mail.alertcentric.com',
                                    "forwardEmail": "enabled",
                                    'group': 'Printers',
                                    'createDate': 1438820000000,
                                    'groupId': '55ba12c964b69a18085b3ec0',
                                    'id': {
                                        '_bsontype': 'ObjectID',
                                        'id': 'Uº\u0012âd¶\u0018\b[>Á'
                                    },
                                    "lastEmailReceive": "21st Oct - 12:58am",
                                    'name': 'HP Inkjet S422',
                                    'product': 'Printer',
                                    'productId': '559fb1e9bdda2d4e7afe4310',
                                    'status': 'other',
                                    'vendor': 'HP',
                                    'vendorId': '559faffb504387237880abce'
                                }
                            ],
                            'title': 'Reports'
                        }
                    ];
                    assert.deepEqual(compileMock.args[0], expectedToFirstUser);
                    assert.equal(setTosMock.args[0], 'austraila@contact.com');
                    assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                    assert.equal(setSubjectMock.args[0], 'Reports');
                    assert.equal(sendMock.callCount, 1);
                    assert.equal(setHtmlMock.args[0], 'Mock html function in jade');
                    done();
                });
            });
        });
        describe('and user set send reports on Sundays', function ()
        {
            describe('when is sunday', function ()
            {
                beforeEach(function (done)
                {
                    setDate(561200000 + 4 * 3600000 * 24);
                    sendReports().then(function ()
                    {
                        done()
                    });
                });
                it('should sent reports', function (done)
                {
                    assert.strictEqual(compileFileMock.callCount, 1);
                    assert.deepEqual(compileFileMock.args[0], ['./app/templates/reports.tpl.jade', {pretty: true}]);
                    assert.equal(setSubjectMock.args[0], 'Reports');
                    assert.equal(sendMock.callCount, 1);
                    assert.equal(setHtmlMock.args[0], 'Mock html function in jade');
                    var expect = [
                        {
                            'delay': [],
                            'interactionRequired': [],
                            'notDetected': [],
                            'title': 'Reports'
                        }
                    ];
                    assert.deepEqual(compileMock.args[0], expect);
                    done();
                });
            });
            describe('when is other day', function ()
            {
                beforeEach(function (done)
                {
                    setDate(561200000 + 3 * 3600000 * 24);
                    sendReports().then(function ()
                    {
                        done()
                    });
                });
                it('should not sent report', function (done)
                {
                    assert.strictEqual(compileFileMock.callCount, 0);
                    assert.strictEqual(setTosMock.callCount, 0);
                    assert.strictEqual(sendMock.callCount, 0);
                    assert.strictEqual(setFromMock.callCount, 0);
                    assert.strictEqual(setSubjectMock.callCount, 0);
                    assert.strictEqual(compileMock.callCount, 0);
                    assert.strictEqual(setHtmlMock.callCount, 0);
                    done();
                });
            });
        });
    });
});

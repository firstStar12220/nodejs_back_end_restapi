describe('findDelayDevice', function ()
{
    'use strict';

    var proxyquire = require('proxyquire');
    var testHelper = require('../../testHelper'),
            setDate = require('../../../app/services/dateHelper').set,
            sinon = require('sinon'),
            assert = require('assert');
    var setTosMock, setHtmlMock, setTextMock, setFromMock, setSubjectMock, EmailMock, sendMock, findDelayDevices, sendgridMock;

    beforeEach(function ()
    {
        sendgridMock = sinon.spy(function ()
        {
            return {
                send: sendMock,
                Email: EmailMock
            }
        });
        sendMock = sinon.spy(function (data, callback)
        {
            callback();
        });
        setTosMock = sinon.spy();
        setHtmlMock = sinon.spy();
        setTextMock = sinon.spy();
        setFromMock = sinon.spy();
        setSubjectMock = sinon.spy();
        EmailMock = sinon.spy(function ()
        {
            return {
                setTos: setTosMock,
                setHtml: setHtmlMock,
                setText: setTextMock,
                setFrom: setFromMock,
                setSubject: setSubjectMock
            }
        });

        findDelayDevices = proxyquire('../../../app/business/sendgrid.manager', {
            sendgrid: sendgridMock
        }).findDelayDevices;
        return testHelper.openDBConnection()
                .then(testHelper.readFileToSeed)
                .then(testHelper.seedUsers)
                .then(testHelper.seedAlert)
                .then(testHelper.seedGroups)
                .then(testHelper.seedPassword)
                .then(testHelper.seedProduct)
                .then(testHelper.seedVendor)
                .then(testHelper.seedDevices);
    });
    afterEach(function (done)
    {
        testHelper.closeDBConnection(done);
    });
    describe('when in system exist user where is midnight', function ()
    {

        describe('and user have delay device', function ()
        {
            beforeEach(function ()
            {
                setDate(1438820000000);
                return findDelayDevices();
            });
            it('should send email to device', function ()
            {
                assert.strictEqual(sendMock.callCount, 2);
            });
            it('should send data', function ()
            {
                assert.equal(setTosMock.args[0], 'test@example.com');
                assert.equal(setSubjectMock.args[0][0], 'Alert!!!');
                assert.equal(setTextMock.args[0][0], 'Alert Centric did not receive an email within expected time for device Spectre W230 under group Servers');
                assert.equal(setHtmlMock.args[0][0],
                        'Alert Centric did not receive an email within expected time for device <b>Spectre W230</b> under group <b>Servers</b>');
                assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
            });
        });
        describe('and user don\'t have delay device', function ()
        {
            beforeEach(function ()
            {
                setDate(1442439534522);
                return findDelayDevices();
            });
            it('should not send email', function ()
            {
                assert.strictEqual(sendMock.callCount, 0);
            });
        });
        describe('when device is delayed', function ()
        {
            describe('and send email after midnight', function ()
            {
                beforeEach(function ()
                {
                    setDate(1445349534522);
                    return findDelayDevices();
                });
                it('should send email', function ()
                {
                    assert.strictEqual(sendMock.callCount, 1);
                });
            });
        });
    });
    describe('custom tests', function ()
    {
        var data = require('../../fixtures/customTest.data.json');
        beforeEach(function ()
        {
            return testHelper.seedDevices(null, data.devices);
        });
        describe('when in UTC is midnight', function ()
        {
            describe('on mondays', function ()
            {
                beforeEach(function ()
                {
                    setDate(1442793821272 + 86400000);
                    return findDelayDevices();
                });
                it('should send email with device', function ()
                {
                    assert.strictEqual(sendMock.callCount, 3);
                    assert.equal(setTosMock.args[0], 'test@example.com');
                    assert.equal(setSubjectMock.args[0][0], 'Alert!!!');
                    assert.equal(setTextMock.args[0][0],
                            'Alert Centric did not receive an email within expected time for device on Mondays no email UTC under group Servers');
                    assert.equal(setHtmlMock.args[0][0],
                            'Alert Centric did not receive an email within expected time for device <b>on Mondays no email UTC</b> under group <b>Servers</b>');
                    assert.equal(setFromMock.args[0], 'noreply@mail.alertcentric.com');
                });
            });
            describe('on tuesdays', function ()
            {
                beforeEach(function ()
                {
                    setDate(1442793821272 + 2 * 86400000);
                    return findDelayDevices();
                });
                it('should send email with device', function ()
                {
                    assert.strictEqual(sendMock.callCount, 5);
                    assert.equal(setTosMock.args[0], 'test@example.com');
                    assert.equal(setSubjectMock.args[0], 'Alert!!!');
                    assert.equal(setTextMock.args[0],
                            'Alert Centric did not receive an email within expected time for device on Tuesdays no email UTC under group Servers');
                    assert.equal(setTextMock.args[1],
                            'Alert Centric did not receive an email within expected time for device bi weekly no email UTC under group Servers');
                    assert.equal(setTextMock.args[2],
                            'Alert Centric did not receive an email within expected time for device mon-friday email UTC under group Servers');
                });
            });
            describe('on wednesdays', function ()
            {
                beforeEach(function ()
                {
                    setDate(1442793821272 + 3 * 86400000);
                    return findDelayDevices();
                });
                it('should send email with device', function ()
                {
                    assert.strictEqual(sendMock.callCount, 5);
                    assert.equal(setTosMock.args[0], 'test@example.com');
                    assert.equal(setSubjectMock.args[0], 'Alert!!!');
                    assert.equal(setTextMock.args[0],
                            'Alert Centric did not receive an email within expected time for device on Wednesdays no email UTC under group Servers');
                    assert.equal(setTextMock.args[1],
                            'Alert Centric did not receive an email within expected time for device bi weekly no email UTC under group Servers');
                    assert.equal(setTextMock.args[2],
                            'Alert Centric did not receive an email within expected time for device mon-friday email UTC under group Servers');
                });
            });
        });
        describe('on thursdays', function ()
        {
            beforeEach(function ()
            {
                //friday
                setDate(1442793821272 + 4 * 86400000);
                return findDelayDevices();
            });
            it('should send email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 5);
                assert.equal(setTosMock.args[0], 'test@example.com');
                assert.equal(setSubjectMock.args[0], 'Alert!!!');
                assert.equal(setTextMock.args[0],
                        'Alert Centric did not receive an email within expected time for device on Thursdays no email UTC under group Servers');
                assert.equal(setTextMock.args[1],
                        'Alert Centric did not receive an email within expected time for device bi weekly no email UTC under group Servers');
                assert.equal(setTextMock.args[2],
                        'Alert Centric did not receive an email within expected time for device mon-friday email UTC under group Servers');
            });
        });
        describe('on fridays', function ()
        {
            beforeEach(function ()
            {
                //saturday
                setDate(1442793821272 + 5 * 86400000);
                return findDelayDevices();
            });
            it('should send email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 4);
                assert.equal(setTosMock.args[0], 'test@example.com');
                assert.equal(setSubjectMock.args[0], 'Alert!!!');
                assert.equal(setTextMock.args[0],
                        'Alert Centric did not receive an email within expected time for device bi weekly no email UTC under group Servers');
                assert.equal(setTextMock.args[1],
                        'Alert Centric did not receive an email within expected time for device mon-friday email UTC under group Servers');
            });
        });
        describe('on saturdays', function ()
        {
            beforeEach(function ()
            {
                //sunday
                setDate(1442793821272 + 6 * 86400000);
                return findDelayDevices();
            });
            it('should send email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 3);
                assert.equal(setTosMock.args[0], 'test@example.com');
                assert.equal(setSubjectMock.args[0], 'Alert!!!');
                assert.equal(setTextMock.args[0],
                        'Alert Centric did not receive an email within expected time for device on Saturdays on email UTC under group Servers');

            });
        });
        describe('on sunday', function ()
        {
            beforeEach(function ()
            {
                //monday
                setDate(1442793821272 + 7 * 86400000);
                return findDelayDevices();
            });
            it('should send email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 2);
            });
        });
        describe('bi weekly', function ()
        {
            beforeEach(function ()
            {
                setDate(1442793821272 - 14 * 86400000);
                return findDelayDevices();
            });
            it('should send email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 2);
            });
        });
        describe('monthly', function ()
        {
            beforeEach(function ()
            {
                setDate(1442793821272 - 16 * 86400000);
                return findDelayDevices();
            });
            it('should sent email with device', function ()
            {
                assert.strictEqual(sendMock.callCount, 4);
            });
        });
    });
});

(function ()
{
    'use strict';
    var mongoose = require('mongoose-q')();
    var database = require('./../app/config/config').database.resource;
    var Promise = require('bluebird');
    var fs = require('fs');
    var seed;

    function readFileToSeed()
    {
        return new Promise(function (resolve, reject)
        {

            fs.readFile('./seed/seedData.json', {encoding: 'UTF-8'}, function (error, data)
            {
                if (error) {
                    reject(error);
                }
                seed = JSON.parse(data);
                resolve(seed);
            });
        })

    }


    function seedUsers(data, users)
    {
        if (mongoose.connection.collections.user) {
            mongoose.connection.collections.user.drop();
        }
        var userSchema = mongoose.models.user;
        userSchema = userSchema || mongoose.model('users', require('../app/DAO/UserDAO').schema);
        return userSchema.createQ(users || seed.users);
    }

    function seedDevices(data, devices)
    {
        if (mongoose.connection.collections.device) {
            mongoose.connection.collections.device.drop();
        }
        var userSchema = mongoose.models.device;
        userSchema = userSchema || mongoose.model('device', require('../app/DAO/DeviceDAO').schema);
        return userSchema.createQ(devices || seed.device);
    }

    function seedAlert(data, alerts)
    {
        if (mongoose.connection.collections.alert) {
            mongoose.connection.collections.alert.drop();
        }
        var userSchema = mongoose.models.alert;
        userSchema = userSchema || mongoose.model('alert', require('../app/DAO/AlertDAO').schema);
        return userSchema.createQ(alerts || seed.alert);
    }

    function seedGroups(data, groups)
    {
        if (mongoose.connection.collections.groups) {
            mongoose.connection.collections.groups.drop();
        }
        var userSchema = mongoose.models.groups;
        userSchema = userSchema || mongoose.model('groups', require('../app/DAO/GroupDAO').schema);
        return userSchema.createQ(groups || seed.groups);
    }

    function seedVendor(data, vendors)
    {
        if (mongoose.connection.collections.vendor) {
            mongoose.connection.collections.vendor.drop();
        }
        var userSchema = mongoose.models.vendor;
        userSchema = userSchema || mongoose.model('vendor', require('../app/DAO/VendorDAO').schema);
        return userSchema.createQ(vendors || seed.vendor);
    }

    function seedProduct(data,products)
    {
        if (mongoose.connection.collections.product) {
            mongoose.connection.collections.product.drop();
        }
        var userSchema = mongoose.models.product;
        userSchema = userSchema || mongoose.model('product', require('../app/DAO/ProductDAO').schema);
        return userSchema.createQ(products || seed.product);
    }

    function seedPassword()
    {
        if (mongoose.connection.collections.password) {
            mongoose.connection.collections.password.drop();
        }
        var userSchema = mongoose.models.password;
        userSchema = userSchema || mongoose.model('password', require('../app/DAO/PasswordDAO').schema);
        return userSchema.createQ(seed.password);
    }

    function clearCollection(collectionName)
    {
        return new Promise(function (resolve)
        {
            mongoose.connection.collections[collectionName].drop();
            resolve();
        })
    }

    function getEmailModel()
    {
        return mongoose.models.email || mongoose.model('email', require('../app/DAO/EmailDAO').schema);
    }

    function getDeviceModel()
    {
        return mongoose.models.device || mongoose.model('device', require('../app/DAO/DeviceDAO').schema);
    }

    function openDBConnection()
    {
        return new Promise(function (resolve, reject)
        {

            mongoose.connect(database, function (error)
            {
                if (error) {
                    console.log(error);
                    reject(error);
                }
                mongoose.connection.db.dropDatabase();
                resolve();
            });
        })

    }

    function closeDBConnection(done)
    {
        //sometimes need this function
        //mongoose.connection.db.dropDatabase();
        mongoose.connection.close(function (error)
        {
            if (error) {
                done(error);
            }
            done();
        });
    }


    module.exports = {
        openDBConnection: openDBConnection,
        closeDBConnection: closeDBConnection,
        seedUsers: seedUsers,
        seedAlert: seedAlert,
        seedGroups: seedGroups,
        seedVendor: seedVendor,
        seedProduct: seedProduct,
        seedPassword: seedPassword,
        clearCollection: clearCollection,
        getEmailModel: getEmailModel,
        seedDevices: seedDevices,
        readFileToSeed: readFileToSeed,
        getDeviceModel: getDeviceModel
    };
})();
